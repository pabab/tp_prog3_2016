# Programación de Videojuegos III 2016

## FICH - UNL

### [Idea / Nombre Pendiente]

Juego casual en el que el jugador debe utilizar la pantalla táctil para mover 2 moléculas por la pantalla de manera simultánea utilizando 2 dedos. También sería posible jugar de a 2.

Cuando el jugador está arrastrando las moléculas, se genera un arco eléctrico entre ellas, el jugador debe utilizar éste arco para destruir enemigos que aparecen desde los costados de la pantalla. Si el jugador suelta alguna de las dos moléculas el arco eléctrico desaparece, ésto puede ser útil para generar mecánicas como por ejemplo que existan ciertos objetos que haya que evitar tocar con el arco eléctrico o que la energía del arco sea limitada y haya que procurar ahorrarla.

#### Qué está implementado

* Las partículas se mueven con arrastrándolas con el dedo o mouse
* Se genera el arco eléctrico entre las partículas
* Se generan enemigos
* Los enemigos mueren al colisionar con el arco eléctrico
* Se lleva un puntaje con los enemigos liquidados

#### Qué está faltando implementar

* Que haya un núcleo que las partículas deban proteger de los enemigos
* Que pueda haber más de dos partículas (los arcos eléctricos se generarían entre cualesquiera dos partículas que se arrastren)
* Que los enemigos aparezcan de distintas direcciones (algunos vayan a atacar al núcleo, otros a las partículas)
* Que si un enemigo toca a una partícula ésta quede inhabilitada durante un tiempo y cambie su trayectoria (empiece a moverse)

