
extends Node2D

var timeToNextEnemy = 0
var scoreLabel
var score = 0
var particles = []
var rays = []

const EnemyClass = preload("res://scripts/enemy.gd")

func report_hit(obj):
	print(obj.get_name())
	if obj extends EnemyClass:
		remove_child(obj)
		score = score + 1 
		scoreLabel.set_text("SCORE: "+str(score))

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process(true)
	scoreLabel = get_node("ScoreLabel")
	scoreLabel.set_text("SCORE: 0")
	createParticles(4)
	createRays()
	for r in rays:
		add_child(r)
	for p in particles:
		add_child(p)

func createParticles(nParticles):
	var radius = 200
	var angleStep = 2*PI/nParticles;
	var angle = 0;
	for i in range(0, nParticles):
		var particle = load("res://scenes/particula.scn").instance()
		particle.set_pos(Vector2(200+radius*cos(angle), 200+radius*sin(angle)))
		particles.append(particle)
		angle = angle + angleStep
		
	

func createRays():
	for i in range(0, particles.size()):
		for j in range(i+1, particles.size()):
			var ray = load("res://scenes/ray.scn").instance()
			ray.init(particles[i], particles[j])
			rays.append(ray)

func _process(delta):
	timeToNextEnemy -= delta
	if timeToNextEnemy <= 0:
		timeToNextEnemy = randi()%5
		var enemigo = load("res://scenes/enemy.scn").instance()
		enemigo.set_pos(Vector2(randi()%640, -32));
		self.add_child(enemigo)
	
	


