
extends RigidBody2D

var touchIndex = -1
export var touchDistance = 50

func isBeingTouched():
	return touchIndex != -1

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	self.set_process_input(true)
	
func _input(event):
	if event.type == InputEvent.SCREEN_TOUCH:
		if event.pressed and get_pos().distance_to(Vector2(event.pos)) <= touchDistance:
			touchIndex = event.index
		elif not event.pressed and event.index == touchIndex:
			touchIndex = -1
	elif event.type == InputEvent.SCREEN_DRAG and event.index == touchIndex:
		self.set_pos(Vector2(event.x, event.y))
	

