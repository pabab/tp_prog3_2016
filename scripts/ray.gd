
extends Node2D

# var b="textvar"
var particle1
var particle2
var raySprite
var frame = 0
var timeToChangeFrame = 0
var rayCast

func _ready():
	set_fixed_process(true)

func init(p1, p2):
	raySprite = get_node("RaySprite")
	rayCast = get_node("RayCast2D")
	particle1 = p1
	particle2 = p2
	rayCast.add_exception(particle1)
	rayCast.add_exception(particle2)

func _fixed_process(delta):
	if particle1.isBeingTouched() or particle2.isBeingTouched():
		raySprite.show()
		var dist = particle1.get_pos().distance_to(particle2.get_pos())
		var dir = (particle2.get_pos()-particle1.get_pos()).normalized()
		var angle = particle1.get_pos().angle_to_point(particle2.get_pos())
		raySprite.set_rot(angle+PI/2.0)
		raySprite.set_pos(particle1.get_pos()+dir*dist/2)
		timeToChangeFrame += delta;
		if timeToChangeFrame >= 0.025:
			timeToChangeFrame = 0
			var nextFrame = frame
			while nextFrame == frame:
				nextFrame = randi()%6
			frame = nextFrame
		raySprite.frame = frame
		raySprite.set_region_rect(Rect2(0, 128*frame, dist, 128))
		rayCast.set_pos(particle1.get_pos())
		rayCast.set_cast_to(particle2.get_pos())
		rayCast.set_enabled(true)
		var space_state = get_world_2d().get_direct_space_state()
		# use global coordinates, not local to node
		var result = space_state.intersect_ray(particle1.get_pos(), particle2.get_pos() )
		#if rayCast.is_colliding():
		if not result.empty():
			#var obj = rayCast.get_collider()
			get_parent().report_hit(result.collider)
	else:
		raySprite.hide()
		rayCast.set_enabled(false)
